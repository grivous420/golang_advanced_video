module golang_advanced_video

go 1.14

require (
	github.com/ilyakaznacheev/cleanenv v1.2.6
	github.com/julienschmidt/httprouter v1.3.0
	github.com/sirupsen/logrus v1.8.1
	go.mongodb.org/mongo-driver v1.8.4
)
